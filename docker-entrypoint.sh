#!/bin/bash

cp -R /var/www/tmp/laravel-kubernetes/. /var/www/html/
chown -R www-data:www-data /var/www/html

exec "$@"